﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArreglosParalelosOrdenados
{
    class MainPaisesHabitantes
    {
        static void Main(string[] args)
        {
            PaisesHabitantes p = new PaisesHabitantes();

            p.SolicitarCantidadPaises();

            p.SolicitarInformacionPaises();

            p.MenuOrdenamiento();

            p.MostrarInformacionPaises();

            Console.WriteLine("\n\nPresione cualquier tecla para salir de la aplicación...");

            Console.ReadKey();
        }
    }
}