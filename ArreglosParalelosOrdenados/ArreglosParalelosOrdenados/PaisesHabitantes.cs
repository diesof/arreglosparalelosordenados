﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArreglosParalelosOrdenados
{
    class PaisesHabitantes
    {
        private string[] paises;
        private int[] habitantes;
        private int cantidadPaises;

        public void SolicitarCantidadPaises()
        {
            Console.Write("¿Cuántos países dará de alta?: ");
            cantidadPaises = int.Parse(Console.ReadLine());

            paises = new string[cantidadPaises];
            habitantes = new int[cantidadPaises];
        }

        public void SolicitarInformacionPaises()
        {
            for (int i = 0; i < paises.Length; i++)
            {
                Console.Write("\nIngrese el país: ");
                paises[i] = Console.ReadLine();

                Console.Write($"Ingrese el número de habitantes del país {paises[i]}: ");
                habitantes[i] = int.Parse(Console.ReadLine());
            }
        }

        private void OrdenarPaisesAlfabeticamente()
        {
            string auxPaises;
            int auxHabitantes;

            for (int i = 0; i < paises.Length; i++)
            {
                for (int j = 0; j < paises.Length; j++)
                {
                    if (paises[i].CompareTo(paises[j]) < 0)
                    {
                        auxPaises = paises[i];
                        paises[i] = paises[j];
                        paises[j] = auxPaises;

                        auxHabitantes = habitantes[i];
                        habitantes[i] = habitantes[j];
                        habitantes[j] = auxHabitantes;
                    }
                }
            }
        }

        private void OrdenarHabitantesMayorMenor()
        {
            string auxPaises;
            int auxHabitantes;

            for (int i = 0; i < paises.Length; i++)
            {
                for (int j = 0; j < paises.Length; j++)
                {
                    if (habitantes[i] > habitantes[j])
                    {
                        auxHabitantes = habitantes[i];
                        habitantes[i] = habitantes[j];
                        habitantes[j] = auxHabitantes;

                        auxPaises = paises[i];
                        paises[i] = paises[j];
                        paises[j] = auxPaises;
                    }
                }
            }
        }

        public void MenuOrdenamiento()
        {
            int opcionOrdenamiento;

            Console.WriteLine("\n-------------Menú Ordenamiento-------------");
            Console.WriteLine("(1)Ordenar por países Alfabéticamente");
            Console.WriteLine("(2)Ordenar por habitantes de Mayor a Menor");
            Console.WriteLine("-------------------------------------------");

            do
            {
                Console.Write("¿Como desea ordenar la información?: ");
                opcionOrdenamiento = int.Parse(Console.ReadLine());

            } while (opcionOrdenamiento != 1 && opcionOrdenamiento != 2);

            switch (opcionOrdenamiento)
            {
                case 1:
                    OrdenarPaisesAlfabeticamente();
                    break;

                case 2:
                    OrdenarHabitantesMayorMenor();
                    break;
            }
        }

        public void MostrarInformacionPaises()
        {
            for (int i = 0; i < paises.Length; i++)
            {
                if (i == 0)
                {
                    Console.Write("\n\n<<<<<<<<<<Información de los países>>>>>>>>>>");
                }
                Console.WriteLine($"\nPaís: {paises[i].ToUpper()}");
                Console.WriteLine($"Habitantes: {habitantes[i]}");
                Console.WriteLine("---------------------------------------------");

                if (i == paises.Length - 1)
                {
                    Console.WriteLine("---------------------------------------------");
                }
            }

            Console.WriteLine($"Cantidad de países procesados: {cantidadPaises}");
        }
    }
}